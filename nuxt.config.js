export default {
  head: {
    titleTemplate: '%s - Mediun',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Meta description' },
    ],
  },
  ssr: false,
  components: true,
  target: 'static',
  srcDir: 'client/',
  buildDir: '.core',
  server: {
    port: 3000,
    host: '0',
  },
  auth: {
    redirect: {
      login: false,
      logout: false,
      callback: false,
      home: '/app/home',
    },
    strategies: {
      local: {
        token: {
          property: 'token',
          type: 'Bearer',
          global: true,
          maxAge: 86400,
        },
        user: {
          property: 'results',
        },
        endpoints: {
          login: { url: '/api/login', method: 'post' },
          logout: { url: '/api/logout', method: 'post' },
          user: { url: '/api/me', method: 'get' },
        },
      },
    },
  },
  
  router: {
    middleware: ['auth'],
  },
  modules: ['@nuxtjs/axios', '@nuxtjs/toast', '@nuxtjs/auth-next'],
  buildModules: ['@nuxtjs/vuetify'],
  axios: {
    baseURL: process.env.API_GATEWAY
      ? process.env.API_GATEWAY
      : 'https://supergateway.jhc.asia/api',
  },
  toast: {
    position: 'bottom-center',
    duration: 3000,
  },
  vuetify: {
    treeShake: true,
    defaultAssets: {
      font: {
        family: 'Montserrat',
      },
    },
    theme: {
      light: true,
      themes: {
        light: {
          primary: '#16347A',
          accent: '#526CFF',
          secondary: '#F2994A',
          info: '#06A0C1',
          warning: '#F2994A',
          error: '#EB5757',
          success: '#7BBC78',
        },
      },
    },
  },
}
